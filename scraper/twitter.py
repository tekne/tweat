import tweepy
import os
from dotenv import load_dotenv
import pandas as pd
from typing import List
load_dotenv()

auth = tweepy.OAuthHandler(os.getenv("CONSUMER_KEY"), os.getenv("CONSUMER_SECRET"))
auth.set_access_token(os.getenv("ACCESS_TOKEN"), os.getenv("ACCESS_TOKEN_SECRET"))

api = tweepy.API(auth)


def get_tweets(query_terms: List[str], count: int = 100):
    tweets = []

    for query in query_terms:
        tweets += api.search(query, count=count)

    tweets = [vars(tweet) for tweet in tweets]
    tweets = pd.DataFrame(tweets)
    tweets = tweets.drop(['_api', '_json'], axis=1).drop_duplicates('created_at')

    return tweets


if __name__ == '__main__':
    n = 100
    uniswap = get_tweets(['#Uniswap'], count=n)
    bitcoin = get_tweets(['#Bitcoin', '#BTC', 'BTC'], count=n)
